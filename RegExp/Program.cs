﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RegExp
{
    class Program
    {
        /// <summary>
        /// В поисках идеального RegExp для URL:
        /// https://mathiasbynens.be/demo/url-regex
        /// можно былобы взять пять лидеров, и получить от них списки.
        /// вывести только те, которые встречаются как минимум в трех списках
        /// </summary>


        static void Main(string[] args)
        {
            string ss;
            if (args.Length > 0)
                ss = args[0];
            else
                ss = "https://otus.ru/learning/38253/#/";

            string s = GetBodyUrl(ss);

            Regex regex = new Regex(@"((http[s]?|ftp):\/{2})?[-a-zA-Z0-9:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9():%_\+.~#?&//=]*)?");
            MatchCollection matches = regex.Matches(s);
            HashSet<string> hs = new HashSet<string>();
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    string url = match.Value;
                    //выведем только абсолютные uri
                    if (Uri.IsWellFormedUriString(url, UriKind.Absolute))
                    {
                        if (!hs.Contains(url))//дубликаты не выводим
                        {
                            hs.Add(url);
                            Console.WriteLine(url);
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Совпадений не найдено");
            }

            Console.ReadKey();
        }

        public static string GetBodyUrl(string address)
        {
            WebClient client = new WebClient();
            client.Credentials = CredentialCache.DefaultNetworkCredentials;
            return client.DownloadString(address);
        }

    }
}
